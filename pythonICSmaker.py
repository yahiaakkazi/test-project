import os
from ics import Calendar, Event
try:
    CalendarTitle=input("Enter the title of the calendar")

except EOFError as e:
    CalendarTitle="TestJenkins"

try:
    CalendarStartDateAndTime=input('Enter the start date and time in the following format: AAAA-MM-DD 00:00:00')

except EOFError as e:
    CalendarStartDateAndTime="2021-07-07 01:00:00"

try:
    CalendarEndDateAndTime=input('Enter the end date and time in the following format: AAAA-MM-DD 00:00:00')

except EOFError as e:
    CalendarEndDateAndTime="2021-07-07 02:00:00"

try:
    CalendarDescription=input('Enter a description')

except EOFError as e:
    CalendarDescription="JenkinsDescription"

try:
    CalendarLocation=input("Enter the location")

except EOFError as e:
    CalendarLocation="JenkinsLocation"

c = Calendar()
e = Event()
e.name= CalendarTitle
e.begin=CalendarStartDateAndTime
e.end=CalendarEndDateAndTime
e.description=CalendarDescription
e.location=CalendarLocation
c.events.add(e)
c.events
with open('myCalendar.ics', 'w') as my_file:
    my_file.writelines(c)
currentpwd=os.getcwd()
print("Done, your .ics file can be found in "+ currentpwd)
